namespace CC_Flores
{

class Program
	{
		static void Main(string[] args)
		{ 
      string txt; 
			Console.WriteLine("vlož text");
			txt = Console.ReadLine();
			Console.WriteLine("CamelCase úprava:");
			Console.WriteLine(CamelCase(txt));
			Console.ReadKey();
		}
    
		static string CamelCase(string txt)
		{ 
			TextInfo cultInfo = new CultureInfo("en-US", false).TextInfo;
			txt = cultInfo.ToTitleCase(txt);
			if(txt.Contains('_'))
			txt = txt.Replace('_', string.Empty);
			if(txt.Contains(' '))
            txt = txt.Replace(' ', string.Empty);
			return txt;
       }
    }
  }