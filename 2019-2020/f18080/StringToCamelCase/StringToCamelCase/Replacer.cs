﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StringToCamelCase
{
    class Replacer
    {
        public string CamelCase(string text)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            text = textInfo.ToTitleCase(text);
            text = $"{text.First().ToString().ToLowerInvariant()}{text.Substring(1)}";
            text = Regex.Replace(text, @"\s+|_|-", "");
            return text;
        }
    }
}
