﻿using System;
using System.Globalization;

namespace camelCaseHubner
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Zadejte text:");
			string text = Console.ReadLine();
			Console.WriteLine("CamelCase text:");
			Console.WriteLine(CamelCase(text));
			Console.ReadKey();
		}
		static string CamelCase(string txt)
		{
			TextInfo cultInfo = new CultureInfo("en-US", false).TextInfo;
			txt = cultInfo.ToTitleCase(txt);
			txt = txt.Replace(" ", "").Replace("-", "").Replace("_", "");
			return char.ToLower(txt[0]) + txt.Substring(1);
		}
	}
}
