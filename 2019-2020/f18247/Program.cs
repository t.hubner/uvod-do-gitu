﻿using System;
using System.Globalization;
using System.Linq;

namespace camelCase {
    class Program {
        static void Main(string[] args) {
            string input, camel;

            Console.Write("Zadejte váš požadovaný string k převodu: ");
            input = Console.ReadLine();
            camel = CamelCase(input);
            Console.WriteLine("Převedený string je: {0}", camel);
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();

        }

        static string CamelCase(string str) {
            TextInfo text = new CultureInfo("en-us", false).TextInfo;
            str = text.ToTitleCase(str).Replace("_", string.Empty).Replace(" ", string.Empty);
            str = $"{str.First().ToString().ToLowerInvariant()}{str.Substring(1)}";
            return str;
        }
    }
}