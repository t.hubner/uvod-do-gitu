using System;
using System.Text.RegularExpressions;


namespace camelcasify
{
    class CamelCassify
    {

        static string camelCasify(string input)
        {

            if (input.Length == 0) return "input is null";
            char[] input_chars = input.ToCharArray();

            Regex regex = new Regex(@"(_|\W+)|([A-Z])");
            MatchCollection matches = regex.Matches(input);

            foreach (Match match in matches)
            {
                GroupCollection groups = match.Groups; 
                if (groups[1].Index != 0 && input.Length > (groups[1].Index + 1))
                {

                    input_chars[groups[1].Index + 1] = char.ToUpper(input_chars[groups[1].Index + 1]);
                    input_chars[groups[1].Index] = ' ';

                }              
            }
            input = new string(input_chars);
            input = char.ToLower(input[0]) + input.Substring(1);
            return input.Replace(" ", "");

        }
        static void Main(string[] args)
        {
            string input;
            bool control = true;

            while (control)
            {
                Console.WriteLine("y - Zvelbloudovat string \nn - konec programu");
                input = Console.ReadLine();
                Console.Clear();
                switch (input)
                {
                    case "y":
                        Console.WriteLine("Napište string k zvelbloudování:\n");
                        input = Console.ReadLine();
                        Console.WriteLine("Zvelbloudovaný string:\n" + camelCasify(input) + "\n");
                        break;
                    case "n":
                        control = false;
                        break;
                    default:
                        break;

                }
            }
        }




    }
}
