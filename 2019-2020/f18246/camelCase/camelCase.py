def convert_to_camelCase(text):
    """
    Metoda převádí text na camelCase
    :param text: řetězec, který má být převeden
    :return: řetězec ve tvaru camelCase
    """

    flag = False
    result = ''

    for char in text:
        if(char.isalnum() and flag):
            result = result + char.upper()
            flag = False
        elif (char.isalnum()):
            result = result + char
        else:
            flag = True

    result = result[0].lower() + result[1:]

    return result


if __name__ == "__main__":

    TEXT = 'Nevím co sem napsat'
    camelCase_string = convert_to_camelCase(text=TEXT)
    print(camelCase_string)

    TEXT = 'Nevím_co_sem_napsat'
    camelCase_string = convert_to_camelCase(text=TEXT)
    print(camelCase_string)

    TEXT = 'Nevím-co-sem-napsat'
    camelCase_string = convert_to_camelCase(text=TEXT)
    print(camelCase_string)

    TEXT = 'NevímCoSemNapsat'
    camelCase_string = convert_to_camelCase(text=TEXT)
    print(camelCase_string)