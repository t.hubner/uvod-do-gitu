def string_to_camelCase(input):
    import re
    input = str(input)
    my_list = re.split(r'[-_\s]\s*', input)

    list_len = len(my_list)
    final_list = ''

    for i in range(list_len):
        if i == 0:
            tmp = list(my_list[i])
            tmp[0] = tmp[0].lower()
            my_list[i] = ''
            for x in tmp:
                my_list[i] += x
        else:
            tmp = list(my_list[i])
            tmp[0] = tmp[0].upper()
            my_list[i] = ''
            for x in tmp:
                my_list[i] += x

    for y in my_list:
        final_list += y

    return final_list