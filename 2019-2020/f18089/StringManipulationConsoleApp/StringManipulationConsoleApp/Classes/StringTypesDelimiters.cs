﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringManipulationConsoleApp.Classes
{
    public enum StringType
    {
        PascalCase,
        KebabCase,
        SnakeCase,
        Regular
    }

    class StringTypesDelimiters
    {
        public static readonly char RegularCaseDelimiter = ' ';
        public static readonly char KebabCaseDelimiter = '-';
        public static readonly char SnakeCaseDelimiter = '_';
    }
}
