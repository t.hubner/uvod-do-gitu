<?php
$canSave = true;
if(empty($_POST["citace"])){
    exit(header("Location: /index.php?er=empty"));
}
$text = $_POST["citace"];

//camelCase
function CamelCase($string, $bigFirstLetter = false) 
{
    $str = str_replace(' ', '', ucwords(str_replace('-', ' ', strtolower(str_replace('_',' ',$string)))));

    if (!$bigFirstLetter) {
        $str[0] = strtolower($str[0]);
    }

    return $str;
}


if($canSave){
    //vytvoření souboru pokud není
    //$myfile = fopen("list.txt", "a") or die("Unable to open file!");    
    //fclose($myfile);

    //vložení textu do souboru
    file_put_contents("list.txt", CamelCase($text).PHP_EOL, FILE_APPEND);   

    //přesmerování
    header("Location: /index.php");
} else{
    header("Location: /index.php?er=empty");
}

?>