<?php 
$title = "ahoj Dane";
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title><?php echo $title;?></title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="starter-template.css" rel="stylesheet">
  </head>

  <body>

    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="">cameCase citátor</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Hlavní jediná stránka</a>
          </li>                  
        </ul>        
      </div>
    </nav>

    <main role="main" class="container">

      <div class="starter-template">
        <h1>Vítej na této stránce</h1>
        <p class="lead">Zadej svoji citatci nebo svoji větu.<br> Nech moje AI ti to upravit do světa proměn v programování.</p>
      </div>

    <form action="postik.php" method="POST">
        <div class="form-group" >
            <label for="citace">Užásný vkládač</label>
            <input type="text" class="form-control" id="citace" name="citace" placeholder="Zadej citaci">
            <small id="emailHelp" class="form-text text-muted">Ukážu ti kouzlo programování ♥.</small>
        </div>
        <button type="submit" class="btn btn-primary">Vytvořit CamelCase</button>
    </form>

    <br><hr><br>
    <?php 
    if(isset($_GET["er"])){
        echo '<div class="alert alert-danger" role="alert">
        Prosím vložte nějaký text, ja nemůžu upravit něco co neexistuje
      </div>';
    }
    ?>
    <h1>Seznam všech úžasných cameCase názvu</h1>

    <?php
    


    if (file_exists("list.txt") &&filesize('list.txt') == 0) {
        echo "<h2>Ještě nikdo nic nenapsal</h2>";
    }elseif(file_exists("list.txt")){
        echo '<ul class="list-group list-group-flush">';

        $file = fopen("list.txt","r");
            while(!feof($file))
            {
                echo '<li class="list-group-item">'.fgets($file). "</li>";
            }
        fclose($file);

        echo "</ul>";
    }else{
        echo "<h2>Soubor neni ¯\_(ツ)_/¯</h2>";
    }
    
    ?>

    </main><!-- /.container -->

    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
